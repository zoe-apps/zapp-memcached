# Copyright (c) 2016, Daniele Venzano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Spark-Jupyter Zoe application description generator."""

import json
import sys
import os

APP_NAME = 'memcached'
ZOE_APPLICATION_DESCRIPTION_VERSION = 3

options = {
    'memory_limit': {
        'value': 512 * (1024**2),
        'description': 'Memcached memory limit (bytes)'
    }
}

REGISTRY = os.getenv("DOCKER_REGISTRY", default="docker-engine:5000")
REPOSITORY = os.getenv("REPOSITORY", default="zapps")
VERSION = os.getenv("VERSION", default="latest")

IMAGE = "memcached:alpine"

def memcached_service(memory_limit):
    """
    :rtype: dict
    """
    mem_limit_mb = memory_limit / (1024**2)
    service = {
        'name': "memcached",
        'image': IMAGE,
        'monitor': True,
        'resources': {
            "memory": {
                "min": memory_limit,
                "max": memory_limit
            },
            "cores": {
                "min": 1,
                "max": 1
            }
        },
        'ports': [
            {
                'name': "Memcached port",
                'protocol': 'tcp',
                'port_number': 11211,
                'url_template': '{ip_port}'
            }
        ],
        'environment': [],
        'volumes': [],
        'command': 'memcached -m {}m'.format(mem_limit_mb),
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 0
    }

    return service


if __name__ == '__main__':
    app = {
        'name': APP_NAME,
        'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
        'will_end': False,
        'size': 1024,
        'services': [
            memcached_service(options["memory_limit"]["value"])
        ]
    }

    json.dump(app, open("memcached.json", "w"), sort_keys=True, indent=4)

    print("ZApp written")

